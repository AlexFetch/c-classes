﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer4
{
    class Classesworker
    {
        public static void StudInfworker()
        {
            List<StudInfGroup> group = new List<StudInfGroup>
                {
                    new StudInfGroup() {fio="Пупкин Василий Иванович", birthyear=1986, homeadress="Бобруйск", school=3},
                    new StudInfGroup() {fio="Сусанин Иван Степанович", birthyear=1995, homeadress="Сыктывкар", school=3},
                    new StudInfGroup() {fio="Разин Степан Петрович", birthyear=1989, homeadress="Таганрог", school=5},
                    new StudInfGroup() {fio="Иванов Семён Васильевич", birthyear=1993, homeadress="Ревель", school=3},
                    new StudInfGroup() {fio="Горбунков Семён Семёнович", birthyear=1999, homeadress="Чугуево", school=1}
                };
            List<StudInfGroup> sorted = new List<StudInfGroup>
            {
                new StudInfGroup(),
                new StudInfGroup(),
                new StudInfGroup(),
                new StudInfGroup(),
                new StudInfGroup()
            };
            List<StudInfGroup> time = new List<StudInfGroup>{
                new StudInfGroup()
            };
            int maincount, maincount2, sortcount = 0, schoolnum;
            Console.Write("Введите номер законченной школы -> ");
            schoolnum = Convert.ToInt32(Console.ReadLine());
            for (maincount = 0; maincount < group.Count; maincount++)
            {
                if (group[maincount].school == schoolnum)
                {
                    sorted[sortcount] = group[maincount];
                    sortcount++;
                }
            }
            for (maincount = 0; maincount < sortcount - 1; maincount++)
            {
                for (maincount2 = maincount + 1; maincount2 < sortcount; maincount2++)
                {
                    if (sorted[maincount].birthyear > sorted[maincount2].birthyear)
                    {
                        time[0] = sorted[maincount2];
                        sorted[maincount2] = sorted[maincount];
                        sorted[maincount] = time[0];
                    }
                }
            }
            for (maincount = 0; maincount < sortcount; maincount++)
            {
                Console.WriteLine("Студент {0}", maincount);
                Console.WriteLine("ФИО - {0}", sorted[maincount].fio);
                Console.WriteLine("Год рождения - {0}", sorted[maincount].birthyear);
                Console.WriteLine("Домашний адрес - {0}", sorted[maincount].homeadress);
                Console.WriteLine("Школа которую закончил - {0}", sorted[maincount].school);
            }
        }

        public static void StudMarksworker()
        {
            List<StudMarksGroup> group = new List<StudMarksGroup>
                {
                    new StudMarksGroup() {fio="Пупкин Василий Иванович", groupnum=2, markex1=4, markex2=4, markex3=5},
                    new StudMarksGroup() {fio="Сусанин Иван Степанович", groupnum=2, markex1=4, markex2=5, markex3=3},
                    new StudMarksGroup() {fio="Разин Степан Петрович", groupnum=1, markex1=5, markex2=5, markex3=4},
                    new StudMarksGroup() {fio="Иванов Семён Васильевич", groupnum=3, markex1=4, markex2=4, markex3=4},
                    new StudMarksGroup() {fio="Горбунков Семён Семёнович", groupnum=2, markex1=3, markex2=3, markex3=3}
                };
            List<StudMarksGroup> sorted = new List<StudMarksGroup>
            {
                new StudMarksGroup(),
                new StudMarksGroup(),
                new StudMarksGroup(),
                new StudMarksGroup(),
                new StudMarksGroup()
            };
            List<StudMarksGroup> time = new List<StudMarksGroup>{
                new StudMarksGroup()
            };
            int maincount, maincount2, sortcount = 0;
            Console.WriteLine("Вывести информацию -> ");
            Console.ReadKey();
            for (maincount = 0; maincount < group.Count; maincount++)
            {
                if (group[maincount].markex1 > 3 && group[maincount].markex2 > 3 && group[maincount].markex3 > 3)
                {
                    sorted[sortcount] = group[maincount];
                    sortcount++;
                }
            }
            for (maincount = 0; maincount < sortcount - 1; maincount++)
            {
                for (maincount2 = maincount + 1; maincount2 < sortcount; maincount2++)
                {
                    if (sorted[maincount].groupnum > sorted[maincount2].groupnum)
                    {
                        time[0] = sorted[maincount2];
                        sorted[maincount2] = sorted[maincount];
                        sorted[maincount] = time[0];
                    }
                }
            }
            for (maincount = 0; maincount < sortcount; maincount++)
            {
                Console.WriteLine("Студент {0}", maincount);
                Console.WriteLine("ФИО - {0}", sorted[maincount].fio);
                Console.WriteLine("Номер группы - {0}", sorted[maincount].groupnum);
                Console.WriteLine("Оценка за экзамен1 - {0}", sorted[maincount].markex1);
                Console.WriteLine("Оценка за экзамен2 - {0}", sorted[maincount].markex2);
                Console.WriteLine("Оценка за экзамен3 - {0}", sorted[maincount].markex3);
            }
        }

        public static void Luggageworker()
        {
            List<LuggageInf> group = new List<LuggageInf>
                {
                    new LuggageInf() {fio="Пупкин Василий Иванович", lugamount=7, avglugweight=34.8},
                    new LuggageInf() {fio="Сусанин Иван Степанович", lugamount=3, avglugweight=17.2},
                    new LuggageInf() {fio="Разин Степан Петрович", lugamount=1, avglugweight=5.6},
                    new LuggageInf() {fio="Иванов Семён Васильевич", lugamount=9, avglugweight=2.4},
                    new LuggageInf() {fio="Горбунков Семён Семёнович", lugamount=5, avglugweight=11.5}
                };
            List<LuggageInf> sorted = new List<LuggageInf>
            {
                new LuggageInf(),
                new LuggageInf(),
                new LuggageInf(),
                new LuggageInf(),
                new LuggageInf()
            };
            List<LuggageInf> time = new List<LuggageInf>{
                new LuggageInf()
            };
            int maincount, maincount2, sortcount = 0, weight;
            Console.Write("Введите вес багажа -> ");
            weight = Convert.ToInt32(Console.ReadLine());
            for (maincount = 0; maincount < group.Count; maincount++)
            {
                if (group[maincount].avglugweight > weight)
                {
                    sorted[sortcount] = group[maincount];
                    sortcount++;
                }
            }
            for (maincount = 0; maincount < sortcount - 1; maincount++)
            {
                for (maincount2 = maincount + 1; maincount2 < sortcount; maincount2++)
                {
                    if (sorted[maincount].lugamount > sorted[maincount2].lugamount)
                    {
                        time[0] = sorted[maincount2];
                        sorted[maincount2] = sorted[maincount];
                        sorted[maincount] = time[0];
                    }
                }
            }
            for (maincount = 0; maincount < sortcount; maincount++)
            {
                Console.WriteLine("Пассажир {0}", maincount);
                Console.WriteLine("ФИО - {0}", sorted[maincount].fio);
                Console.WriteLine("Количество вещей - {0}", sorted[maincount].lugamount);
                Console.WriteLine("Общий вес вещей - {0}", sorted[maincount].avglugweight);
            }
        }

        public static void Autoworker()
        {
            List<AutoInf> group = new List<AutoInf>
                {
                    new AutoInf() {brand="BMW", number="АА3475СА", ownerfio="Пупкин Василий Иванович", puryear=1996, mileage=34698},
                    new AutoInf() {brand="Mitsubishi", number="ВІ3475АО", ownerfio="Сусанин Иван Степанович", puryear=2002, mileage=24783},
                    new AutoInf() {brand="Таврия", number="з3475КД", ownerfio="Разин Степан Петрович", puryear=1989, mileage=88885},
                    new AutoInf() {brand="Mercedes-Benz", number="ІІ0007ІІ", ownerfio="Иванов Семён Васильевич", puryear=2013, mileage=1000},
                    new AutoInf() {brand="Peugeot", number="ВА3475АА", ownerfio="Горбунков Семён Семёнович", puryear=2012, mileage=3500}
                };
            List<AutoInf> sorted = new List<AutoInf>
            {
                new AutoInf(),
                new AutoInf(),
                new AutoInf(),
                new AutoInf(),
                new AutoInf()
            };
            List<AutoInf> time = new List<AutoInf>{
                new AutoInf()
            };
            int maincount, maincount2, sortcount = 0, year;
            Console.Write("Введите год выпуска -> ");
            year = Convert.ToInt32(Console.ReadLine());
            for (maincount = 0; maincount < group.Count; maincount++)
            {
                if (group[maincount].puryear < year)
                {
                    sorted[sortcount] = group[maincount];
                    sortcount++;
                }
            }
            for (maincount = 0; maincount < sortcount - 1; maincount++)
            {
                for (maincount2 = maincount + 1; maincount2 < sortcount; maincount2++)
                {
                    if (sorted[maincount].mileage > sorted[maincount2].mileage)
                    {
                        time[0] = sorted[maincount2];
                        sorted[maincount2] = sorted[maincount];
                        sorted[maincount] = time[0];
                    }
                }
            }
            for (maincount = 0; maincount < sortcount; maincount++)
            {
                Console.WriteLine("Автомобиль {0}", maincount);
                Console.WriteLine("Марка - {0}", sorted[maincount].brand);
                Console.WriteLine("Номер - {0}", sorted[maincount].number);
                Console.WriteLine("ФИО владельца - {0}", sorted[maincount].ownerfio);
                Console.WriteLine("Год приобретения - {0}", sorted[maincount].puryear);
                Console.WriteLine("Пробег - {0}", sorted[maincount].mileage);
            }
        }
    }
}
