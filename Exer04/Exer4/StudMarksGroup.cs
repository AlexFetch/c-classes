﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer4
{
    class StudMarksGroup
    {
        public string fio { get; set; }
        public int groupnum { get; set; }
        public int markex1 { get; set; }
        public int markex2 { get; set; }
        public int markex3 { get; set; }
    }
}
