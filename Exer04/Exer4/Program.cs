﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer4
{
    // Create 4 classes. Fill lists with objects of classes. Sort lists by parameter.
    class Program
    {
        static void Main(string[] args)
        {
            Classesworker.StudInfworker();
            Classesworker.StudMarksworker();
            Classesworker.Luggageworker();
            Classesworker.Autoworker();
            Console.ReadKey();
        }
    }
}
