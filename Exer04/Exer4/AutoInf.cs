﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer4
{
    class AutoInf
    {
        public string brand { get; set; }
        public string number { get; set; }
        public string ownerfio { get; set; }
        public int puryear { get; set; }
        public int mileage { get; set; }
    }
}
