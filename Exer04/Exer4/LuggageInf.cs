﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer4
{
    class LuggageInf
    {
        public string fio { get; set; }
        public int lugamount { get; set; }
        public double avglugweight { get; set; }
    }
}
