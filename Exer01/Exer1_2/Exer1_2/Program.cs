﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer1_2
{
    // User enters sentence. Reverse word order in it. 
    class Program
    {
        static void Main(string[] args)
        {
            string sentence;
            Console.Write("Enter your sentence -> ");
            sentence = Console.ReadLine();
            Revertor.Reverse(sentence);
            Console.ReadKey();
        }   
    }
}
