﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer1_2
{
    public static class Revertor
    {
        public static void Reverse(string mainsent)
        {
            string timesent, reversestr = "";
            bool strend = false, nullstr = false;
            var strarray = new List<string>();
            int indexspc, count = 0, strarraylen;
            timesent = mainsent;
            if (timesent == null || timesent == "" || timesent == " ")
                nullstr = true;
            if (nullstr == false)
            {
                while (strend == false)
                {
                    indexspc = timesent.IndexOf(" ");
                    while (indexspc == 0)
                    {
                        timesent = timesent.Remove(0, indexspc + 1);
                        indexspc = timesent.IndexOf(" ");
                    }
                    strarray.Add("");
                    indexspc = timesent.IndexOf(" ");
                    if (indexspc != 0 && indexspc != -1)
                    {
                        strarray[count] = timesent.Substring(0, indexspc);
                        timesent = timesent.Remove(0, indexspc + 1);
                        count++;
                    }
                    else
                    {
                        strarray[count] = timesent.Substring(0, timesent.Length);
                        timesent = timesent.Remove(0, timesent.Length);
                    }
                    if (timesent == null || timesent == "" || timesent == " ")
                        strend = true;
                }
                timesent = "";
                count = 0;
                strarraylen = strarray.Count;
                for (count = 0; count < strarraylen / 2 + 1; count++)
                {
                    timesent = strarray[count];
                    strarray[count] = strarray[strarraylen - count - 1];
                    strarray[strarraylen - count - 1] = timesent;
                }
                for (count = 0; count < strarraylen; count++)
                {
                    if (count != strarraylen - 1)
                    {
                        reversestr = reversestr + strarray[count] + " ";
                    }
                    else
                        if (count == strarraylen - 1)
                        {
                            reversestr = reversestr + strarray[count];
                        }
                }
                Console.Write("Reversed sentence is -> {0}", reversestr);
            }
            else
                if (nullstr == true)
                {
                    Console.Write("You entered nothing.");
                }
        }
    }
}
