﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer1_1
{
    // Create structure. User fills structure. Display it.
    class Program
    {
        struct User
        {
            public string UserName;
            public string LastName;
            public string Group;
            public string Age;
        }

        private static void IOuser(User curruser)
        {
            Console.Write("Enter your name -> ");
            curruser.UserName = Console.ReadLine();
            Console.Write("Enter your lastname -> ");
            curruser.LastName = Console.ReadLine();
            Console.Write("Enter your group -> ");
            curruser.Group = Console.ReadLine();
            Console.Write("Enter your age -> ");
            curruser.Age = Console.ReadLine();
            Console.WriteLine("Your name is -> {0}", curruser.UserName);
            Console.WriteLine("Your lastname is -> {0}", curruser.LastName);
            Console.WriteLine("Your group is -> {0}", curruser.Group);
            Console.WriteLine("Your age is -> {0}", curruser.Age);
        }

        static void Main(string[] args)
        {
            User curruser = new User();
            IOuser(curruser);
            Console.ReadKey();
        }
    }
}
