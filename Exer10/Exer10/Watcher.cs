﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Permissions;
using System.Collections;

namespace Exer10
{
    class Watcher
    {
        [PermissionSet(SecurityAction.Demand, Name = "InYouWeTrust")]
        public static void Watch(string path, string exec = "")
        {
            if (exec == "")
            {
                FileSystemWatcher eyeofsauron = new FileSystemWatcher(path);
                eyeofsauron.Changed += new FileSystemEventHandler(OnChanged);
                eyeofsauron.Created += new FileSystemEventHandler(OnChanged);
                eyeofsauron.Deleted += new FileSystemEventHandler(OnChanged);
                eyeofsauron.Renamed += new RenamedEventHandler(OnRenamed);
                eyeofsauron.EnableRaisingEvents = true;
            }
            else
            {
                FileSystemWatcher eyeofsauron = new FileSystemWatcher(path, exec);
                eyeofsauron.Changed += new FileSystemEventHandler(OnChanged);
                eyeofsauron.Created += new FileSystemEventHandler(OnChanged);
                eyeofsauron.Deleted += new FileSystemEventHandler(OnChanged);
                eyeofsauron.Renamed += new RenamedEventHandler(OnRenamed);
                eyeofsauron.EnableRaisingEvents = true;
            }

            Console.WriteLine("Press any key to exit.");
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            Console.WriteLine("Change: File - " + e.FullPath + " " + e.ChangeType);
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            Console.WriteLine("Rename: File - {0} renamed to {1}", e.OldFullPath, e.FullPath);
        }
    }
}
