﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer10
{
    static class ListExt
    {
        public static List<T> ChSort<T>(this List<T> arr) where T : IComparable<T>
        {
            int counti = 0, countj = 0, elind = 0;

            for (counti = 0; counti < arr.Count - 1; counti++)
            {
                var timeel = arr[counti];
                for (countj = counti + 1; countj < arr.Count; countj++)
                {

                    if (timeel.CompareTo(arr[countj]) > 0)
                    {
                        timeel = arr[countj];
                        elind = countj;
                    }
                }
                arr[elind] = arr[counti];
                arr[counti] = timeel;
            }

            return arr;
        }

        public static List<T> ShSort<T>(this List<T> arr) where T : IComparable<T>
        {
            int count = 0, first = 0, last = 0;
            var timeel = arr[0];

            for (count = 0; count < arr.Count / 2; count++)
            {
                first = 0;
                last = arr.Count - 1;

                do
                {
                    if (arr[first].CompareTo(arr[first + 1]) > 0)
                    {
                        timeel = arr[first];
                        arr[first] = arr[first + 1];
                        arr[first + 1] = timeel;
                    }
                    first++;
                    if (arr[last - 1].CompareTo(arr[last]) > 0)
                    {
                        timeel = arr[last - 1];
                        arr[last - 1] = arr[last];
                        arr[last] = timeel;
                    }
                    last--;
                } while (first <= last);
            }

            return arr;
        }
    }
}
