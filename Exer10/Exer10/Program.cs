﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Permissions;
using System.Collections;

namespace Exer10
{
    // 1. Create prgram for watchin file system on desired path. Class Watcher.
    // 2. Create extension methods for List<T> for other methods of sort. Class ListExt.
    class Program
    {
        private static void Beginwatching()
        {
            string pth, exe;
            Console.Write("Enter path to watch for -> ");
            pth = Console.ReadLine();
            Console.Write("Enter file execution to watch for -> ");
            exe = Console.ReadLine();
            if (exe == "")
            {
                Watcher.Watch(pth);
            }
            else
            {
                Watcher.Watch(pth, exe);
            }
        }

        static void Main(string[] args)
        {
            Beginwatching();
        }
    }
}
