﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer2_3
{
    static class Matrixwork
    {
        public static void FSDMatrix(int[,] numarray, int[] sumarray)
        {
            Random randomnum = new Random();
            int counti = 0, countj = 0, countk = 0, time = 0;

            for (counti = 0; counti < numarray.GetUpperBound(0)+1; counti++)
            {
                for (countj = 0; countj < numarray.GetUpperBound(0) + 1; countj++)
                {
                    numarray[counti, countj] = randomnum.Next(-500, 500);
                }
            }
            for (counti = 0; counti < sumarray.Length; counti++)
            {
                for (countj = 0; countj < sumarray.Length; countj++)
                {
                    sumarray[counti] += numarray[countj, counti];
                }
            }

            for (counti = 0; counti < sumarray.Length-1; counti++)
            {
                for (countj = counti + 1; countj < sumarray.Length; countj++)
                {
                    if (sumarray[countj] < sumarray[counti])
                    {
                        time = sumarray[counti];
                        sumarray[counti] = sumarray[countj];
                        sumarray[countj] = time;
                        for (countk = 0; countk < numarray.GetUpperBound(0) + 1; countk++)
                        {
                                time = numarray[countk, counti];
                                numarray[countk, counti] = numarray[countk, countj];
                                numarray[countk, countj] = time;
                        }
                    }
                }
            }
            
            for (counti = 0; counti < sumarray.Length; counti++)
            {
                if (counti == 0)
                {
                    Console.WriteLine("Sum of columns");
                }
                if (sumarray[counti] <= -1000)
                {
                    Console.Write("{0} ", sumarray[counti]);
                }
                else
                    if ((sumarray[counti] >= -999 && sumarray[counti] <= -100) || (sumarray[counti] >= 1000))
                    {
                        Console.Write("{0}  ", sumarray[counti]);
                    }
                    else
                        if ((sumarray[counti] >= -99 && sumarray[counti] <= -10) || (sumarray[counti] >= 100 && sumarray[counti] <= 999))
                        {
                            Console.Write("{0}   ", sumarray[counti]);
                        }
                        else
                            if ((sumarray[counti] >= -9 && sumarray[counti] <= -1) || (sumarray[counti] >= 10 && sumarray[counti] <= 99))
                            {
                                Console.Write("{0}    ", sumarray[counti]);
                            }
                            else
                                if (sumarray[counti] >= 0 && sumarray[counti] <= 9)
                                {
                                    Console.Write("{0}    ", sumarray[counti]);
                                }
                if (counti == sumarray.Length-1)
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Matrix");
                }
            }
            for (counti = 0; counti < numarray.GetUpperBound(0) + 1; counti++)
            {
                for (countj = 0; countj < numarray.GetUpperBound(0) + 1; countj++)
                {
                    if (numarray[counti, countj] <= -100)
                    {
                        Console.Write("{0}  ", numarray[counti, countj]);
                    }
                    else
                        if ((numarray[counti, countj] >= -99 && numarray[counti, countj] <= -10) || (numarray[counti, countj] >= 100))
                        {
                            Console.Write("{0}   ", numarray[counti, countj]);
                        }
                        else
                            if ((numarray[counti, countj] >= -9 && numarray[counti, countj] <= -1) || (numarray[counti, countj] >= 10 && numarray[counti, countj] <= 99))
                            {
                                Console.Write("{0}    ", numarray[counti, countj]);
                            }
                            else
                                if (numarray[counti, countj] >= 0 && numarray[counti, countj] <= 9)
                                {
                                    Console.Write("{0}     ", numarray[counti, countj]);
                                }               
                }
                Console.WriteLine();
            }
                Console.ReadKey();
        }
    }
}
