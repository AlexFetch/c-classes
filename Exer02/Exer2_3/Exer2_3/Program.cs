﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer2_3
{
    class Program
    {
        // Fill square matrix with random numbers. Sort matrix by sum of columns
        static void Main(string[] args)
        {
            int[,] matrix = new int[10, 10];
            int[] colsum = new int[10];

            Matrixwork.FSDMatrix(matrix, colsum);
        }
    }
}
