﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer2_2
{
    // Fill array with random numbers and sort it.
    class Program
    {
        private static void SortArray(int[] numarray)
        {
            Random randomnum = new Random();
            int counti = 0, countj = 0, time = 0;
            for (counti = 0; counti < numarray.Length; counti++)
            {
                numarray[counti] = randomnum.Next(-500, 500);
            }
            for (counti = 0; counti < numarray.Length-1; counti++)
            {
                for (countj = counti + 1; countj < numarray.Length; countj++)
                {
                    if (numarray[countj] < numarray[counti])
                    {
                        time = numarray[counti];
                        numarray[counti] = numarray[countj];
                        numarray[countj] = time;
                    }
                }
            }
            foreach (int elem in numarray)
            {
                Console.WriteLine(elem);
            }
        }

        static void Main(string[] args)
        {
            int[] tosortarray = new int[10];
            SortArray(tosortarray);
            Console.ReadKey();
        }
    }
}
