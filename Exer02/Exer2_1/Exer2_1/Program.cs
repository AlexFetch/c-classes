﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer2_1
{
    // Create structure. Create List of structure objects and sort it via List<T>.Sort() method.
    class Program
    {
        struct Stud
        {
            public string UserName;
            public string LastName;
            public string Group;
            public string Age;
        }

        private static int Comparestr(string one, string two)
        {
            if (one == null)
            {
                if (two == null)
                    return 0;
                else
                    return -1;
            }
            else
            {
                if (two == null)
                    return 1;
                else
                {
                    int comres = one.CompareTo(two);
                    if (comres != 0)
                        return comres;
                    else
                        return one.Length.CompareTo(two.Length);
                }
            }
        }

        private static void FSOList(List<Stud> grup)
        {
            grup.Add(new Stud { UserName = "Vasya", LastName = "Pupkin", Group = "df-27", Age = "27" });
            grup.Add(new Stud { UserName = "Sanya", LastName = "Ivanov", Group = "mn-38", Age = "51" });
            grup.Add(new Stud { UserName = "Misha", LastName = "Karpov", Group = "er-12", Age = "45" });
            grup.Sort((x, y) => Comparestr(x.LastName, y.LastName));
            Console.WriteLine(grup[0].LastName);
            Console.WriteLine(grup[1].LastName);
            Console.WriteLine(grup[2].LastName);
        }

        static void Main(string[] args)
        {
            List<Stud> studgroup = new List<Stud>();
            FSOList(studgroup);
            Console.ReadKey();
        }
    }
}
