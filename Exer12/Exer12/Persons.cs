﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Exer12
{
    // 6
    class Persons : IEnumerable<Person>
    {
        List<Person> list = new List<Person>();
        int curInd;

        public void Add(Person p)
        {
            list.Add(p);
        }

        public IEnumerator<Person> GetEnumerator()
        {
            for (curInd = 0; curInd < list.Count; curInd++)
            {
                yield return list[curInd];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
