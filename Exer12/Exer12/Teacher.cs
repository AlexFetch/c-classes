﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer12
{
    class Teacher : Person, /*4*/ICloneable
    {
        public string kafedra { get; set; }
        public string stud { get; set; }
        public double stav { get; set; }

        public override void Print(string stv)
        {
            Console.WriteLine(kafedra);
            Console.WriteLine(stud);
            Console.WriteLine(stv);
        }

        public string ToString(double conv)
        {
            return Convert.ToString(conv);
        }

        public bool Equals(Object obj)
        {
            if (obj == null || !(obj is Teacher))
            {
                return false;
            }
            return true;
        }

        public int GetHashCode()
        {
            return Convert.ToInt32(Math.Pow(stav, 2));
        }

        public static Teacher RandomPerson(Teacher[] timearr)
        {
            int arrnum = 0;
            Random rand = new Random();
            arrnum = rand.Next(0, timearr.Length - 1);
            return timearr[arrnum];
        }

        //4
        public Object Clone()
        {
            Teacher cloned = (Teacher)this.MemberwiseClone();
            cloned.kafedra = this.kafedra;
            cloned.stud = this.stud;
            cloned.stav = this.stav;
            return cloned;
        }
    }
}
