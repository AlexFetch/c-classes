﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer12
{
    class Classeswork
    {
        public static void Studentsort()
        {
            // 2
            Student[] studarr = {new Student(){teach="a", course=2},
                               new Student(){teach="b", course=3},
                               new Student(){teach="c", course=1}};
            Array.Sort(studarr);
            foreach (Student std in studarr)
            {
                Console.Write("{0}   ", std.teach);
                Console.WriteLine(std.course);
            }
        }

        public static void Studentsortpar()
        {
            // 3
            Student[] studarr = {new Student(){teach="b", course=2},
                               new Student(){teach="a", course=3},
                               new Student(){teach="c", course=1}};
            Student.SortByTeach = true;
            Student.StudentComparer stcompt = new Student.StudentComparer(Student.SortByTeach, Student.SortByCourse);
            Array.Sort<Student>(studarr, stcompt);
            foreach (Student std in studarr)
            {
                Console.Write("{0}   ", std.teach);
                Console.WriteLine(std.course);
            }
            Console.WriteLine();
            Student.SortByTeach = false;
            Student.SortByCourse = true;
            Student.StudentComparer stcompc = new Student.StudentComparer(Student.SortByTeach, Student.SortByCourse);
            Array.Sort<Student>(studarr, stcompc);
            foreach (Student std in studarr)
            {
                Console.Write("{0}   ", std.teach);
                Console.WriteLine(std.course);
            }
        }

        public static void Studentfinder()
        {
            // 6
            int count = 1;
            Persons perss = new Persons();
            perss.Add(new Person() { fio = "Pupkin Vasiliy Ivanovich", age = 25, sex = "male" });
            perss.Add(new Person() { fio = "Ivanov Semen Semenovich", age = 45, sex = "male" });
            perss.Add(new Student() { teach = "a", course = 3 });
            perss.Add(new Student() { teach = "b", course = 2 });
            foreach (Person pers in perss)
            {
                Console.WriteLine("{0} ", count);
                if (pers is Student)
                {
                    Student stud = pers as Student;
                    Console.WriteLine("  {0} {1}", stud.teach, stud.course);
                }
                else
                    if (pers is Person)
                    {
                        Console.WriteLine("  {0} {1} {2}", pers.fio, pers.age, pers.sex);
                    }
                count++;
            }
        }
    }
}
