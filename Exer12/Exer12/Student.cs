﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer12
{
    class Student : Person, /*1*/IPrintable, /*2*/IComparable<Student>, /*4*/ICloneable
    {
        public string teach { get; set; }
        public int course { get; set; }

        // 3
        public static bool SortByTeach { get; set; }
        public static bool SortByCourse { get; set; }

        public override void Print(string cour)
        {
            Console.WriteLine(teach);
            Console.WriteLine(cour);
        }

        public string ToString(int conv)
        {
            return Convert.ToString(conv);
        }

        public bool Equals(Object obj)
        {
            if (obj == null || !(obj is Student))
            {
                return false;
            }
            return true;
        }

        public int GetHashCode()
        {
            return Convert.ToInt32(Math.Pow(course, 2));
        }

        public static Student RandomPerson(Student[] timearr)
        {
            int arrnum = 0;
            Random rand = new Random();
            arrnum = rand.Next(0, timearr.Length - 1);
            return timearr[arrnum];
        }

        // 1
        public void Printer(string topr)
        {
            Console.WriteLine(teach);
            Console.WriteLine(topr);
        }

        // 2
        public int CompareTo(Student std)
        {
            if (course > std.course)
            {
                return 1;
            }
            else
                if (course < std.course)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
        }

        // 3
        public class StudentComparer : IComparer<Student>
        {
            bool sortbyteach;
            bool sortbycourse;

            public StudentComparer(bool _sortbyteach = false, bool _sortbycourse = false)
            {
                sortbyteach = _sortbyteach;
                sortbycourse = _sortbycourse;
            }

            public int Compare(Student stud1, Student stud2)
            {
                if (sortbyteach == true)
                {
                    if (stud1.teach.CompareTo(stud2.teach) > 0)
                    {
                        return 1;
                    }
                    else
                        if (stud1.teach.CompareTo(stud2.teach) < 0)
                        {
                            return -1;
                        }
                        else
                        {
                            return 0;
                        }
                }
                else
                    if (sortbycourse == true)
                    {
                        if (stud1.course > stud2.course)
                        {
                            return 1;
                        }
                        else
                            if (stud1.course < stud2.course)
                            {
                                return -1;
                            }
                            else
                            {
                                return 0;
                            }
                    }
                    else
                    {
                        return -2;
                    }
            }
        }

        // 4
        public Object Clone()
        {
            Student cloned = (Student)this.MemberwiseClone();
            cloned.teach = this.teach;
            cloned.course = this.course;
            return cloned;
        }
    }
}
