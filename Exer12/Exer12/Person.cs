﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer12
{
    class Person : /*1*/IPrintable, /*4*/ICloneable, /*5*/IDisposable
    {
        public string fio { get; set; }
        public int age { get; set; }
        public string sex { get; set; }

        public virtual void Print(string ag)
        {
            Console.WriteLine(fio);
            Console.WriteLine(ag);
            Console.WriteLine(sex);
        }

        public string ToString(int conv)
        {
            return Convert.ToString(conv);
        }

        public bool Equals(Object obj)
        {
            if (obj == null || !(obj is Person))
            {
                return false;
            }
            return true;
        }

        public int GetHashCode()
        {
            return Convert.ToInt32(Math.Pow(age, 2));
        }

        public static Person RandomPerson(Person[] timearr)
        {
            int arrnum = 0;
            Random rand = new Random();
            arrnum = rand.Next(0, timearr.Length - 1);
            return timearr[arrnum];
        }

        public virtual Person Cloned()
        {
            Person cloned = (Person)this.MemberwiseClone();
            cloned.fio = this.fio;
            cloned.sex = this.sex;
            return cloned;
        }

        // 1
        public void Printer(string topr)
        {
            Console.WriteLine(fio);
            Console.WriteLine(topr);
            Console.WriteLine(sex);
        }

        // 4
        public Object Clone()
        {
            Person cloned = (Person)this.MemberwiseClone();
            cloned.fio = this.fio;
            cloned.age = this.age;
            cloned.sex = this.sex;
            return cloned;
        }

        // 5
        public void Dispose()
        {
            Console.WriteLine("Person disposed");
        }
    }
}
