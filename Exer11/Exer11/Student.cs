﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer11
{
    class Student : Person
    {
        public string teach { get; set; }
        public int course { get; set; }

        public override void Print(string cour)
        {
            Console.WriteLine(teach);
            Console.WriteLine(cour);
        }

        public string ToString(int conv)
        {
            return Convert.ToString(conv);
        }

        public bool Equals(Object obj)
        {
            if (obj == null || !(obj is Student))
            {
                return false;
            }
            return true;
        }

        public int GetHashCode()
        {
            return Convert.ToInt32(Math.Pow(course, 2));
        }

        public static Student RandomPerson(Student[] timearr)
        {
            int arrnum = 0;
            Random rand = new Random();
            arrnum = rand.Next(0, timearr.Length - 1);
            return timearr[arrnum];
        }

        public static bool operator ==(Student obj1, Student obj2)
        {
            if (obj1.course == obj2.course && obj1.teach == obj2.teach)
            {
                return true;
            }
            return false;
        }

        public static bool operator !=(Student obj1, Student obj2)
        {
            if (obj1.course == obj2.course && obj1.teach == obj2.teach)
            {
                return false;
            }
            return true;
        }
    }
}
