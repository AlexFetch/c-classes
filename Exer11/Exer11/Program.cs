﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer11
{
    // 1. Override operators == and != for Person-Student-Teacher classes.
    // 2. Create structure for working with complex numbers and override +,-,*,/,== and != operators.
    class Program
    {
        public struct Complex
        {
            int numx, numy;

            public static Complex operator +(Complex obj1, Complex obj2)
            {
                var compl = new Complex();
                compl.numx = obj1.numx + obj2.numx;
                compl.numy = obj1.numy + obj2.numy;
                return compl;
            }

            public static Complex operator -(Complex obj1, Complex obj2)
            {
                var compl = new Complex();
                compl.numx = obj1.numx - obj2.numx;
                compl.numy = obj1.numy - obj2.numy;
                return compl;
            }

            public static Complex operator *(Complex obj1, Complex obj2)
            {
                var compl = new Complex();
                compl.numx = obj1.numx * obj2.numx - obj1.numy * obj2.numy;
                compl.numy = obj1.numx * obj2.numy + obj1.numy * obj2.numx;
                return compl;
            }

            public static Complex operator /(Complex obj1, Complex obj2)
            {
                var compl = new Complex();
                compl.numx = Convert.ToInt32((obj1.numx * obj2.numx + obj1.numy * obj2.numy) / (Math.Pow(obj2.numx, 2) + Math.Pow(obj2.numy, 2)));
                compl.numy = Convert.ToInt32((obj1.numy * obj2.numx + obj1.numx * obj2.numy) / (Math.Pow(obj2.numx, 2) + Math.Pow(obj2.numy, 2)));
                return compl;
            }

            public static bool operator ==(Complex obj1, Complex obj2)
            {
                if (obj1.numx == obj2.numx && obj1.numy == obj2.numy)
                {
                    return true;
                }
                return false;
            }

            public static bool operator !=(Complex obj1, Complex obj2)
            {
                if (obj1.numx == obj2.numx && obj1.numy == obj2.numy)
                {
                    return false;
                }
                return true;
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return false;
                }

                var comp = (Complex)obj;
                if (numx == comp.numx && numy == comp.numy)
                {
                    return true;
                }

                return false;
            }

            public override string ToString()
            {
                string result = "";
                result = Convert.ToString(numx) + "+" + Convert.ToString(numy) + "i";
                return result;
            }

            public Complex ToComplex(Double num)
            {
                string strvers = Convert.ToString(num), strx, stry;
                int index = strvers.IndexOf(".");
                Complex comp = new Complex();
                strx = strvers.Substring(0, index);
                comp.numx = Convert.ToInt32(strx);
                stry = strvers.Substring(index + 1);
                comp.numy = Convert.ToInt32(stry);
                return comp;
            }

        }

        static void Main(string[] args)
        {
            double dobb = 23.58;
            Complex comp = new Complex();
            comp = comp.ToComplex(dobb);
            Console.WriteLine(comp);
            Console.ReadKey();
        }
    }
}
