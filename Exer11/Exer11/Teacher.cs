﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer11
{
    class Teacher : Person
    {
        public string kafedra { get; set; }
        public string stud { get; set; }
        public double stav { get; set; }

        public override void Print(string stv)
        {
            Console.WriteLine(kafedra);
            Console.WriteLine(stud);
            Console.WriteLine(stv);
        }

        public string ToString(double conv)
        {
            return Convert.ToString(conv);
        }

        public bool Equals(Object obj)
        {
            if (obj == null || !(obj is Teacher))
            {
                return false;
            }
            return true;
        }

        public int GetHashCode()
        {
            return Convert.ToInt32(Math.Pow(stav, 2));
        }

        public static Teacher RandomPerson(Teacher[] timearr)
        {
            int arrnum = 0;
            Random rand = new Random();
            arrnum = rand.Next(0, timearr.Length - 1);
            return timearr[arrnum];
        }

        public static bool operator ==(Teacher obj1, Teacher obj2)
        {
            if (obj1.kafedra == obj2.kafedra && obj1.stud == obj2.stud)
            {
                return true;
            }
            return false;
        }

        public static bool operator !=(Teacher obj1, Teacher obj2)
        {
            if (obj1.kafedra == obj2.kafedra && obj1.stud == obj2.stud)
            {
                return false;
            }
            return true;
        }
    }
}
