﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer8
{
    class ChildE : Empty, IDisposable
    {
        System.Drawing.Font anyfont = new System.Drawing.Font("Times New Roman", 14.0f);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ChildE()
        {
            Dispose(false);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                anyfont.Dispose();
            }
        }
    }
}
