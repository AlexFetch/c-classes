﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer8
{
    class ChildF : Full
    {
        System.Drawing.Font anyfont = new System.Drawing.Font("Times New Roman", 14.0f);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ChildF()
        {
            Dispose(false);
        }

        public override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    anyfont.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }
    }
}
