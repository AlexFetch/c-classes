﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer5
{
    class Personsworker
    {
        public static void Testtostring()
        {
            Person[] perarr = {new Person(){fio="A B C", age=25, sex="male"},
                               new Person(){fio="D E F", age=21, sex="female"},
                               new Person(){fio="G H I", age=23, sex="female"},
                               new Person(){fio="J K L", age=50, sex="male"},
                               new Person(){fio="M N O", age=37, sex="male"}};
            Teacher[] teacharr = {new Teacher(){kafedra="ra", stud="a", stav=1},
                               new Teacher(){kafedra="kis", stud="c", stav=0.5},
                               new Teacher(){kafedra="ius", stud="b", stav=0.5},
                               new Teacher(){kafedra="saue", stud="b", stav=1},
                               new Teacher(){kafedra="mark", stud="a", stav=1}};
            Student[] studarr = {new Student(){teach="a", course=2},
                               new Student(){teach="b", course=3},
                               new Student(){teach="c", course=1}};
            int count = 0;
            for (count = 0; count < perarr.Length; count++)
            {
                perarr[count].Print(perarr[count].ToString(perarr[count].age));
            }
            for (count = 0; count < teacharr.Length; count++)
            {
                teacharr[count].Print(teacharr[count].ToString(teacharr[count].stav));
            }
            for (count = 0; count < studarr.Length; count++)
            {
                studarr[count].Print(studarr[count].ToString(studarr[count].course));
            }
        }

        public static void Persdispclone()
        {
            Object[] perarr = {new Person(){fio="A B C", age=25, sex="male"},
                               new Person(){fio="D E F", age=21, sex="female"},
                               new Person(){fio="G H I", age=23, sex="female"},
                               new Person(){fio="J K L", age=50, sex="male"},
                               new Person(){fio="M N O", age=37, sex="male"},
                               new Teacher(){kafedra="ra", stud="a", stav=1},
                               new Teacher(){kafedra="kis", stud="c", stav=0.5},
                               new Teacher(){kafedra="ius", stud="b", stav=0.5},
                               new Teacher(){kafedra="saue", stud="b", stav=1},
                               new Teacher(){kafedra="mark", stud="a", stav=1},
                               new Student(){teach="a", course=2},
                               new Student(){teach="b", course=3},
                               new Student(){teach="c", course=1}};
            int count = 0, persamo = 0, teachamo = 0, studamo = 0;
            for (count = 0; count < perarr.Length; count++)
            {
                if (perarr[count] is Student)
                {
                    studamo++;
                    Student stud = perarr[count] as Student;
                    stud.course++;
                    perarr[count] = stud;
                    Console.WriteLine("Stud -> {0}, Teacher -> {1}, Course -> {2}", studamo, stud.teach, stud.course);
                    continue;
                }
                else
                    if (perarr[count] is Teacher)
                    {
                        teachamo++;
                        continue;
                    }
                    else
                        if (perarr[count] is Person)
                        {
                            persamo++;
                            continue;
                        }
            }
            Console.WriteLine("Students -> {0}", studamo);
            Console.WriteLine("Teachers -> {0}", teachamo);
            Console.WriteLine("Persons -> {0}", persamo);
            Person toclone = perarr[0] as Person;
            Person clonedo = toclone.Clone();
            Console.WriteLine("Person clone");
            Console.WriteLine("FIO -> {0}", clonedo.fio);
            Console.WriteLine("Age -> {0}", clonedo.age);
            Console.WriteLine("Sex -> {0}", clonedo.sex);
            GetFather(perarr);
        }

        static void GetFather(Object[] objarray)
        {
            int count = 0;
            Student t = new Student();
            Type tp = t.GetType();
            for (count = 0; count < objarray.Length; count++)
            {
                if (tp.BaseType == objarray[count].GetType())
                {
                    Console.WriteLine("Person {0}", count);
                    Console.WriteLine("FIO -> {0}", (objarray[count] as Person).fio);
                    Console.WriteLine("Age -> {0}", (objarray[count] as Person).age);
                    Console.WriteLine("Sex -> {0}", (objarray[count] as Person).sex);
                }
            }
        }
    }
}
