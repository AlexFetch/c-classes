﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer5
{
    class Person
    {
        public string fio { get; set; }
        public int age { get; set; }
        public string sex { get; set; }

        public virtual void Print(string ag)
        {
            Console.WriteLine(fio);
            Console.WriteLine(ag);
            Console.WriteLine(sex);
        }

        public string ToString(int conv)
        {
            return Convert.ToString(conv);
        }

        public bool Equals(Object obj)
        {
            if (obj == null || !(obj is Person))
            {
                return false;
            }
            return true;
        }

        public int GetHashCode()
        {
            return Convert.ToInt32(Math.Pow(age, 2));
        }

        public static Person RandomPerson(Person[] timearr)
        {
            int arrnum = 0;
            Random rand = new Random();
            arrnum = rand.Next(0, timearr.Length - 1);
            return timearr[arrnum];
        }

        public virtual Person Clone()
        {
            Person cloned = (Person)this.MemberwiseClone();
            cloned.fio = this.fio;
            cloned.sex = this.sex;
            return cloned;
        }
    }
}
