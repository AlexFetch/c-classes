﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exer5
{
    // Create Person-Student-Teacher classes hierarchy. Override ToString, GetHashCode, Equals methods.
    class Program
    {
        static void Main(string[] args)
        {
            Personsworker.Testtostring();
            Personsworker.Persdispclone();
            Console.ReadKey();
        }
    }
}
