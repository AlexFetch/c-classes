﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Exer13
{
    // Create extension methods for IEnumerable<T>. First method must sort any collection with interface. Second with delegate.
    interface ICompareme<T>
    {
        int Compare(T objcom, T objtocom);
    }

    public delegate int ComDelegate<T>(T obj1, T obj2);

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
