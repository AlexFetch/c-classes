﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Exer13
{
    public static class IEnuExt
    {
        public static IEnumerable<T>[] ChSortIn<T>(this IEnumerable<T>[] arr, Compareror<T> comp) where T : IComparable<T>
        {
            int counti = 0, countj = 0, elind = 0;

            for (counti = 0; counti < arr.Length - 1; counti++)
            {
                var timeel = arr[counti];
                for (countj = counti + 1; countj < arr.Length; countj++)
                {

                    if (comp.Compare((T)timeel, (T)arr[countj]) > 0)
                    {
                        timeel = arr[countj];
                        elind = countj;
                    }
                }
                arr[elind] = arr[counti];
                arr[counti] = timeel;
            }

            return arr;
        }

        public static IEnumerable<T>[] ChSortDel<T>(this IEnumerable<T>[] arr, ComDelegate<T> del) where T : IComparable<T>
        {
            int counti = 0, countj = 0, elind = 0;

            del += Comparerdel<T>.Compare;

            for (counti = 0; counti < arr.Length - 1; counti++)
            {
                var timeel = arr[counti];
                for (countj = counti + 1; countj < arr.Length; countj++)
                {

                    if (del((T)timeel, (T)arr[countj]) > 0)
                    {
                        timeel = arr[countj];
                        elind = countj;
                    }
                }
                arr[elind] = arr[counti];
                arr[counti] = timeel;
            }

            return arr;
        }
    }
}
