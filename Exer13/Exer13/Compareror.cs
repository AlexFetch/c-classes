﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Exer13
{
    public class Compareror<T> : ICompareme<T> where T : IComparable<T>
    {
        public int Compare(T obj1, T obj2)
        {
            if (obj1.CompareTo(obj2) > 0)
                return 1;
            else
                if (obj1.CompareTo(obj2) < 0)
                    return -1;
                else
                    return 0;
        }
    }
}
