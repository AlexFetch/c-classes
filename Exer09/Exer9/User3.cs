﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer9
{
    class User3
    {
        public User3(User1 usron, User2 usrtw)
        {
            usron.RaiseMyEvent1 += HandleMyEvent1;
            usrtw.RaiseMyEvent2 += HandleMyEvent2;
        }

        void HandleMyEvent1(object sender, MyEventArgs e)
        {
            Console.WriteLine("User1 -> User3");
        }

        void HandleMyEvent2(object sender, MyEventArgs e)
        {
            Console.WriteLine("User2 -> User3");
        }

        public event EventHandler<MyEventArgs> RaiseMyEvent3;

        public void DoIt()
        {
            OnRaiseMyEvent3(new MyEventArgs());
        }

        protected virtual void OnRaiseMyEvent3(MyEventArgs e)
        {
            EventHandler<MyEventArgs> handler = RaiseMyEvent3;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
