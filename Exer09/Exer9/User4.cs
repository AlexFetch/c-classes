﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer9
{
    class User4
    {
        public User4(User1 usron, User2 usrtw, User3 usrth)
        {
            usron.RaiseMyEvent1 += HandleMyEvent1;
            usrtw.RaiseMyEvent2 += HandleMyEvent2;
            usrth.RaiseMyEvent3 += HandleMyEvent3;
        }

        void HandleMyEvent1(object sender, MyEventArgs e)
        {
            Console.WriteLine("User1 -> User4");
        }

        void HandleMyEvent2(object sender, MyEventArgs e)
        {
            Console.WriteLine("User2 -> User4");
        }

        void HandleMyEvent3(object sender, MyEventArgs e)
        {
            Console.WriteLine("User3 -> User4");
        }
    }
}
