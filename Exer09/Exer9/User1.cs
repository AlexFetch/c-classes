﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer9
{
    class User1
    {
        public event EventHandler<MyEventArgs> RaiseMyEvent1;

        public void DoIt()
        {
            OnRaiseMyEvent1(new MyEventArgs());
        }

        protected virtual void OnRaiseMyEvent1(MyEventArgs e)
        {
            EventHandler<MyEventArgs> handler = RaiseMyEvent1;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
