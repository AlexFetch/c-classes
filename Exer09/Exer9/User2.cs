﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer9
{
    class User2
    {
        public User2(User1 usr)
        {
            usr.RaiseMyEvent1 += HandleMyEvent1;
        }

        void HandleMyEvent1(object sender, MyEventArgs e)
        {
            Console.WriteLine("User1 -> User2");
        }

        public event EventHandler<MyEventArgs> RaiseMyEvent2;

        public void DoIt()
        {
            OnRaiseMyEvent2(new MyEventArgs());
        }

        protected virtual void OnRaiseMyEvent2(MyEventArgs e)
        {
            EventHandler<MyEventArgs> handler = RaiseMyEvent2;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
