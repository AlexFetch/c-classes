﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer9
{
    // Create classes for event raising and listening.
    class Program
    {
        static void Main(string[] args)
        {
            User1 userone = new User1();
            User2 usertwo = new User2(userone);
            User3 userthree = new User3(userone, usertwo);
            User4 userfour = new User4(userone, usertwo, userthree);

            userone.DoIt();
            usertwo.DoIt();
            userthree.DoIt();
            Console.ReadLine();
        }
    }
}
