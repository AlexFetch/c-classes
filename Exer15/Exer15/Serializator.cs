﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Exer15
{
    class Serializator
    {
        public static void Serialize()
        {
            List<StudInf> studinfo = new List<StudInf>
                {
                    new StudInf() {fio="Сусанин Иван Степанович", birthyear=1995, homeadress="Сыктывкар", school=3},
                    new StudInf() {fio="Разин Степан Петрович", birthyear=1989, homeadress="Таганрог", school=5}
                };
            List<StudInfGroup> studgroup = new List<StudInfGroup>
                {
                    new StudInfGroup() {fio="Пупкин Василий Иванович", groupnum=3, markex1=4, markex2=4, markex3=5},
                    new StudInfGroup() {fio="Горбунков Семён Семёнович", groupnum=2, markex1=4, markex2=5, markex3=3}
                };
            List<LuggageInf> luginfo = new List<LuggageInf>
                {
                    new LuggageInf() {fio="Петров Виктор Игнатьевич", lugamount=3, avglugweight=17.2},
                    new LuggageInf() {fio="Иванов Семён Васильевич", lugamount=9, avglugweight=2.4}
                };
            List<AutoInf> autogroup = new List<AutoInf>
                {
                    new AutoInf() {brand="BMW", number="АА3475СА", ownerfio="Пупкин Василий Иванович", puryear=1996, mileage=34698},
                    new AutoInf() {brand="Таврия", number="з3475КД", ownerfio="Разин Степан Петрович", puryear=1989, mileage=88885},
                };

            FileStream binstr = new FileStream("SerialBin.bin", FileMode.Create);
            BinaryFormatter binfor = new BinaryFormatter();
            binfor.Serialize(binstr, studinfo);
            binfor.Serialize(binstr, studgroup);
            binfor.Serialize(binstr, luginfo);
            binfor.Serialize(binstr, autogroup);
            binstr.Close();

            FileStream xmlstr = new FileStream("SerialXML.xml", FileMode.Create);
            XmlSerializer xmlser = new XmlSerializer(typeof(StudInf));
            xmlser.Serialize(xmlstr, studinfo);
            xmlser = new XmlSerializer(typeof(StudInfGroup));
            xmlser.Serialize(xmlstr, studgroup);
            xmlser = new XmlSerializer(typeof(LuggageInf));
            xmlser.Serialize(xmlstr, luginfo);
            xmlser = new XmlSerializer(typeof(AutoInf));
            xmlser.Serialize(xmlstr, autogroup);
            xmlstr.Close();

            FileStream soapstr = new FileStream("SerialSoap.xml", FileMode.Create);
            SoapFormatter soapser = new SoapFormatter();
            soapser.Serialize(soapstr, studinfo);
            soapser.Serialize(soapstr, studgroup);
            soapser.Serialize(soapstr, luginfo);
            soapser.Serialize(soapstr, autogroup);
            soapstr.Close();
        }
    }
}
