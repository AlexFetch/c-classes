﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Exer15
{
    [Serializable]
    class StudInf
    {
        public string fio { get; set; }
        public int birthyear { get; set; }
        public string homeadress { get; set; }
        public int school { get; set; }

        public StudInf()
        {
        }

        public StudInf(string _fio, int _birthyear, string _homeadress, int _school)
        {
            fio = _fio;
            birthyear = _birthyear;
            homeadress = _homeadress;
            school = _school;
        }
    }
}
