﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Exer15
{
    [Serializable]
    class LuggageInf
    {
        public string fio { get; set; }
        public int lugamount { get; set; }
        public double avglugweight { get; set; }

        public LuggageInf()
        {
        }

        public LuggageInf(string _fio, int _lugamount, double _avglugweight)
        {
            fio = _fio;
            lugamount = _lugamount;
            avglugweight = _avglugweight;
        }
    }
}
