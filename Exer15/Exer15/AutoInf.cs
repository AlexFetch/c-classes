﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Exer15
{
    [Serializable]
    class AutoInf
    {
        public string brand { get; set; }
        public string number { get; set; }
        public string ownerfio { get; set; }
        public int puryear { get; set; }
        public int mileage { get; set; }

        public AutoInf()
        {
        }

        public AutoInf(string _brand, string _number, string _ownerfio, int _puryear, int _mileage)
        {
            brand = _brand;
            number = _number;
            ownerfio = _ownerfio;
            puryear = _puryear;
            mileage = _mileage;
        }
    }
}
