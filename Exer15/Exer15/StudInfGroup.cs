﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Exer15
{
    [Serializable]
    class StudInfGroup
    {
        public string fio { get; set; }
        public int groupnum { get; set; }
        public int markex1 { get; set; }
        public int markex2 { get; set; }
        public int markex3 { get; set; }

        public StudInfGroup()
        {
        }

        public StudInfGroup(string _fio, int _groupnum, int _markex1, int _markex2, int _markex3)
        {
            fio = _fio;
            groupnum = _groupnum;
            markex1 = _markex1;
            markex2 = _markex2;
            markex3 = _markex3;
        }
    }
}
