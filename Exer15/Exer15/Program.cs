﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Exer15
{
    // Make serialization in binary, SOAP and xml for Exercise4.
    class Program
    {
        static void Main(string[] args)
        {
            Serializator.Serialize();
        }
    }
}
