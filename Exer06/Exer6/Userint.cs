﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer6
{
    class Userint
    {

        public static Dictionary<string, string>[,] Initialize(Dictionary<string, string>[,] cat)
        {
            cat[0, 0].Add("Artist", "Queen");
            cat[0, 0].Add("Album", "One");
            cat[0, 0].Add("Track1", "Song1");
            cat[0, 0].Add("Track2", "Song2");
            cat[0, 0].Add("Track3", "Song3");
            cat[0, 0].Add("Track4", "Song4");
            cat[0, 0].Add("Track5", "Song5");
            cat[0, 1].Add("Artist", "Queen");
            cat[0, 1].Add("Album", "Two");
            cat[0, 1].Add("Track1", "tSong1");
            cat[0, 1].Add("Track2", "tSong2");
            cat[0, 1].Add("Track3", "tSong3");
            cat[0, 1].Add("Track4", "tSong4");
            cat[0, 1].Add("Track5", "tSong5");
            cat[0, 2].Add("Artist", "Queen");
            cat[0, 2].Add("Album", "Three");
            cat[0, 2].Add("Track1", "teSong1");
            cat[0, 2].Add("Track2", "teSong2");
            cat[0, 2].Add("Track3", "teSong3");
            cat[0, 2].Add("Track4", "teSong4");
            cat[0, 2].Add("Track5", "teSong5");
            cat[1, 0].Add("Artist", "Scorpions");
            cat[1, 0].Add("Album", "Arara");
            cat[1, 0].Add("Track1", "aSong1");
            cat[1, 0].Add("Track2", "aSong2");
            cat[1, 0].Add("Track3", "aSong3");
            cat[1, 0].Add("Track4", "aSong4");
            cat[1, 0].Add("Track5", "aSong5");
            cat[1, 1].Add("Artist", "Scorpions");
            cat[1, 1].Add("Album", "Loopa");
            cat[1, 1].Add("Track1", "lSong1");
            cat[1, 1].Add("Track2", "lSong2");
            cat[1, 1].Add("Track3", "lSong3");
            cat[1, 1].Add("Track4", "lSong4");
            cat[1, 1].Add("Track5", "lSong5");
            cat[1, 2].Add("Artist", "Scorpions");
            cat[1, 2].Add("Album", "Faked");
            cat[1, 2].Add("Track1", "fSong1");
            cat[1, 2].Add("Track2", "fSong2");
            cat[1, 2].Add("Track3", "fSong3");
            cat[1, 2].Add("Track4", "fSong4");
            cat[1, 2].Add("Track5", "fSong5");

            return cat;
        }

        public static void Runprog(Dictionary<string, string>[,] cat)
        {
            bool endprogr = false, blres = false;
            string usrchoice = "", arname = "", alname = "", soname = "", userans = "";
            int inres = 0;

            Console.Title = "MC 1.0";
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Music catalogue");
            do
            {
                Console.WriteLine("");
                Console.WriteLine("Main menu");
                Console.WriteLine("1 - Watch catalogue");
                Console.WriteLine("2 - Watch album");
                Console.WriteLine("3 - Add album");
                Console.WriteLine("4 - Delete album");
                Console.WriteLine("5 - Add song");
                Console.WriteLine("6 - Delete song");
                Console.WriteLine("7 - Search for all songs of desired artist");
                Console.WriteLine("8 - Exit");
                Console.Write("Enter number of item -> ");
                usrchoice = Console.ReadLine();
                switch (usrchoice)
                {
                    case "1": Catwork.Catout(cat);
                        break;

                    case "2": Console.Write("Enter name of album -> ");
                        alname = Console.ReadLine();
                        blres = Catwork.Albout(cat, alname);
                        if (!blres)
                        {
                            Console.WriteLine("There is no such album in catalogue. Please enter name of existing album.");
                        }
                        break;

                    case "3": Console.Write("Enter name of artist -> ");
                        arname = Console.ReadLine();
                        Console.Write("Enter name of new album -> ");
                        alname = Console.ReadLine();
                        blres = Catwork.PlusDisc(cat, arname, alname);
                        if (!blres)
                        {
                            Console.WriteLine("There is no such artist in catalogue. Please enter name of existing artist.");
                        }
                        break;

                    case "4": Console.Write("Enter name of artist -> ");
                        arname = Console.ReadLine();
                        Console.Write("Enter name of album -> ");
                        alname = Console.ReadLine();
                        inres = Catwork.MinusDisc(cat, arname, alname);
                        if (inres == 0)
                        {
                            Console.WriteLine("There is no such album in catalogue. Please enter name of existing album.");
                        }
                        else
                            if (inres == -1)
                            {
                                Console.WriteLine("There is no such artist in catalogue. Please enter name of existing artist.");
                            }
                        break;

                    case "5": Console.Write("Enter name of artist -> ");
                        arname = Console.ReadLine();
                        Console.Write("Enter name of album -> ");
                        alname = Console.ReadLine();
                        Console.Write("Enter name of new song -> ");
                        soname = Console.ReadLine();
                        inres = Catwork.PlusSong(cat, arname, alname, soname);
                        if (inres == 0)
                        {
                            Console.WriteLine("There is no such album in catalogue. Please enter name of existing album.");
                        }
                        else
                            if (inres == -1)
                            {
                                Console.WriteLine("There is no such artist in catalogue. Please enter name of existing artist.");
                            }
                        break;

                    case "6": Console.Write("Enter name of artist -> ");
                        arname = Console.ReadLine();
                        Console.Write("Enter name of album -> ");
                        alname = Console.ReadLine();
                        Console.Write("Enter name of song -> ");
                        soname = Console.ReadLine();
                        inres = Catwork.MinusSong(cat, arname, alname, soname);
                        if (inres == 0)
                        {
                            Console.WriteLine("There is no such song in catalogue. Please enter name of existing song.");
                        }
                        else
                            if (inres == -1)
                            {
                                Console.WriteLine("There is no such album in catalogue. Please enter name of existing album.");
                            }
                            else
                                if (inres == -2)
                                {
                                    Console.WriteLine("There is no such artist in catalogue. Please enter name of existing artist.");
                                }
                        break;

                    case "7": Console.Write("Enter name of artist -> ");
                        arname = Console.ReadLine();
                        blres = Catwork.Search(cat, arname);
                        if (!blres)
                        {
                            Console.WriteLine("There is no such artist in catalogue. Please enter name of existing artist.");
                        }
                        break;

                    case "8": Console.WriteLine("Are your sure?");
                        blres = false;
                        do
                        {
                            Console.Write("Plese enter \"E\" or \"e\" to exit or \"M\" or \"m\" to come back to main menu -> ");
                            userans = Console.ReadLine();
                            if (userans == "E" || userans == "e")
                            {
                                endprogr = true;
                                blres = true;
                            }
                            else
                                if (userans == "M" || userans == "m")
                                {
                                    blres = true;
                                }
                                else
                                {
                                    Console.WriteLine("Your entered wrong answer. Please enter correct letter.");
                                }
                        } while (blres == false);
                        break;

                    default: Console.WriteLine("Your entered wrong number. Please enter correct number.");
                        break;
                }
            } while (endprogr == false);
        }
    }
}
