﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exer6
{
    class Catwork
    {
        public static int currentdis { get; set; }

        public static bool PlusDisc(Dictionary<string, string>[,] cat, string art, string alname)
        {
            int index = 0, count = 0;
            switch (art)
            {
                case "Queen": index = cat.GetUpperBound(0);
                    for (count = 0; count < index; count++)
                    {
                        if (cat[0, count].Count == 0)
                        {
                            cat[0, count].Add("Artist", "Queen");
                            cat[0, count].Add("Album", alname);
                            return true;
                        }
                    }
                    cat[0, index + 1] = new Dictionary<string, string> { };
                    cat[0, index + 1].Add("Artist", "Queen");
                    cat[0, index + 1].Add("Album", alname);
                    return true;

                case "Scorpions": index = cat.GetUpperBound(1);
                    for (count = 0; count < index; count++)
                    {
                        if (cat[1, count].Count == 0)
                        {
                            cat[1, count].Add("Artist", "Queen");
                            cat[1, count].Add("Album", alname);
                            return true;
                        }
                    }
                    cat[1, index + 1] = new Dictionary<string, string> { };
                    cat[1, index + 1].Add("Artist", "Scorpions");
                    cat[1, index + 1].Add("Album", alname);
                    return true;

                default: return false;
            }
        }

        public static int MinusDisc(Dictionary<string, string>[,] cat, string art, string alname)
        {
            int index = 0, count = 0;
            switch (art)
            {
                case "Queen": index = cat.GetUpperBound(0);
                    for (count = 0; count < index; count++)
                    {
                        if (cat[0, count].ElementAt(1).Value == alname)
                        {
                            cat[0, count].Clear();
                            return 1;
                        }
                    }
                    return 0;

                case "Scorpions": index = cat.GetUpperBound(1);
                    for (count = 0; count < index; count++)
                    {
                        if (cat[1, count].ElementAt(1).Value == alname)
                        {
                            cat[1, count].Clear();
                            return 1;
                        }
                    }
                    return 0;

                default: return -1;
            }
        }

        public static int PlusSong(Dictionary<string, string>[,] cat, string art, string alname, string soname)
        {
            int index = 0, count = 0;
            string track = "";
            switch (art)
            {
                case "Queen": index = cat.GetUpperBound(0);
                    for (count = 0; count < index; count++)
                    {
                        if (cat[0, count].ElementAt(1).Value == alname)
                        {
                            track = Convert.ToString(cat[0, count].Count);
                            cat[0, count].Add("Track" + track, soname);
                            return 1;
                        }
                    }
                    return 0;

                case "Scorpions": index = cat.GetUpperBound(1);
                    for (count = 0; count < index; count++)
                    {
                        if (cat[1, count].ElementAt(1).Value == alname)
                        {
                            track = Convert.ToString(cat[1, count].Count);
                            cat[1, count].Add("Track" + track, soname);
                            return 1;
                        }
                    }
                    return 0;

                default: return -1;
            }
        }

        public static int MinusSong(Dictionary<string, string>[,] cat, string art, string alname, string soname)
        {
            int index = 0, count = 0, countin = 0;
            string track = "";
            switch (art)
            {
                case "Queen": index = cat.GetUpperBound(0);
                    for (count = 0; count < index; count++)
                    {
                        if (cat[0, count].ElementAt(1).Value == alname)
                        {
                            for (countin = 0; countin < cat[0, count].Count; countin++)
                            {
                                if (cat[0, count].ElementAt(countin).Value == soname)
                                {
                                    track = "Track" + Convert.ToString(countin - 1);
                                    cat[0, count].Remove(track);
                                    return 1;
                                }
                            }
                            return 0;
                        }
                    }
                    return -1;

                case "Scorpions": index = cat.GetUpperBound(1);
                    for (count = 0; count < index; count++)
                    {
                        if (cat[1, count].ElementAt(1).Value == alname)
                        {
                            for (countin = 0; countin < cat[1, count].Count; countin++)
                            {
                                if (cat[1, count].ElementAt(countin).Value == soname)
                                {
                                    track = "Track" + Convert.ToString(countin - 1);
                                    cat[1, count].Remove(track);
                                    return 1;
                                }
                            }
                            return 0;
                        }
                    }
                    return -1;

                default: return -2;
            }
        }

        public static void Catout(Dictionary<string, string>[,] cat)
        {
            int counti = 0, countj = 0, rank = 0;
            rank = cat.Rank;
            for (counti = 0; counti < rank; counti++)
            {
                for (countj = 0; countj <= cat.GetLength(0); countj++)
                {
                    Console.WriteLine("");
                    foreach (KeyValuePair<string, string> pair in cat[counti, countj])
                    {
                        Console.WriteLine("{0} - {1}", pair.Key, pair.Value);
                    }
                }
            }
        }

        public static bool Albout(Dictionary<string, string>[,] cat, string albname)
        {
            int counti = 0, countj = 0;
            for (counti = 0; counti < cat.Rank; counti++)
            {
                for (countj = 0; countj < cat.GetUpperBound(counti); countj++)
                {
                    if (cat[counti, countj].ElementAt(1).Value == albname)
                    {
                        foreach (KeyValuePair<string, string> pair in cat[counti, countj])
                        {
                            Console.WriteLine("{0} - {1}", pair.Key, pair.Value);
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool Search(Dictionary<string, string>[,] cat, string artname)
        {
            int counti = 0, countj = 0;
            for (counti = 0; counti < cat.Rank; counti++)
            {
                if (cat[counti, 0].ElementAt(0).Value == artname)
                {
                    for (countj = 0; countj < cat.GetUpperBound(counti); countj++)
                    {
                        foreach (KeyValuePair<string, string> pair in cat[counti, countj])
                        {
                            Console.WriteLine("{0} - {1}", pair.Key, pair.Value);
                        }
                    }
                    return true;
                }
            }
            return false;
        }
    }
}
