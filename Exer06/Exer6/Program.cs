﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Exer6
{
    // Create music catalogue with ability to add, delete, update song, search songs etc.
    class Program
    {
        static void Main(string[] args)
        {

            Dictionary<string, string>[,] catalog = new Dictionary<string, string>[,] { { new Dictionary<string, string> { }, 
                                                                                          new Dictionary<string, string> { }, 
                                                                                          new Dictionary<string, string> { } }, 
                                                                                        { new Dictionary<string, string> { }, 
                                                                                          new Dictionary<string, string> { }, 
                                                                                          new Dictionary<string, string> { } } };

            catalog = Userint.Initialize(catalog);
            Userint.Runprog(catalog);
            Console.ReadKey();
        }
    }
}
