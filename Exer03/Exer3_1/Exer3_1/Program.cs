﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Exer3_1
{
    // Read numbers form file. There is a couple of strings in file.
    // Numbers are separated with backspaces and letters. Calculate sum of numbers.
    class Program
    {
        private static void Calcsum()
        {
            string[] nummas = File.ReadAllLines(@"..\..\numbers.txt");
            string[] separator = new string[] { " " };
            string[] elem;
            double sum = 0;
            foreach (string str in nummas)
            {
                elem = str.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                foreach (string element in elem)
                {
                    try
                    {
                        sum = sum + Convert.ToDouble(element);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }
            Console.WriteLine(sum);
        }

        static void Main(string[] args)
        {
            Calcsum();
            Console.ReadKey();
        }
    }
}
