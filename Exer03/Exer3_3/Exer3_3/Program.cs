﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Exer3_3
{
    //Display contents of directory(files and subfolders).
    class Program
    {
        private static void PathInfo(string path)
        {
            List<string> directs = new List<string>(Directory.EnumerateDirectories(path));
            List<string> files = new List<string>(Directory.EnumerateFiles(path));
            directs.Sort();
            files.Sort();
            foreach (string elem in directs)
            {
                Console.WriteLine(elem.Substring(elem.LastIndexOf("\\") + 1));
            }
            foreach (string elem in files)
            {
                Console.WriteLine(elem.Substring(elem.LastIndexOf("\\") + 1));
            }
        }

        static void Main(string[] args)
        {
            string pathtoinf = @"..\..\";
            PathInfo(pathtoinf);
            Console.ReadKey();
        }
    }
}
