﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Exer3_2
{
    // Create binary file. Fill it with numbers. Read them and make squared.
    class Program
    {
        private static void Binwork(string name)
        {
            int[] nummas = new int[3];
            BinaryWriter binfil = new BinaryWriter(File.Open(name, FileMode.Create));
            binfil.Write(5);
            binfil.Write(7);
            binfil.Write(9);
            binfil.Flush();
            binfil.Dispose();
            if (File.Exists(name))
            {
                BinaryReader binfl = new BinaryReader(File.Open(name, FileMode.Open));
                for (int count = 0; count < nummas.Length; count++)
                {
                    nummas[count] = binfl.ReadInt32();
                    nummas[count] = Convert.ToInt32(Math.Pow(nummas[count], 2));
                }
                binfl.Dispose();
                BinaryWriter binwrt = new BinaryWriter(File.Open(name, FileMode.Create));
                binwrt.Write(nummas[0]);
                binwrt.Write(nummas[1]);
                binwrt.Write(nummas[2]);
                binwrt.Flush();
                binwrt.Dispose();
                BinaryReader binrd = new BinaryReader(File.Open(name, FileMode.Open));
                for (int count = 0; count < nummas.Length; count++)
                {
                    Console.WriteLine(binrd.ReadInt32());
                }
                binrd.Dispose();
            }
        }

        static void Main(string[] args)
        {
            string filename = "intnums.bin";
            Binwork(filename);
            Console.ReadKey();
        }
    }
}
